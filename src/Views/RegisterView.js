import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';

export default class RegisterView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      firstname   : '',
      lastname   : '',
      phonenumber   : 855,
      email   : '',
      password: ''
    }
  }

  getFirstName(fname) {
    this.setState({ firstname: fname })
  }
  getLastName(lname) {
    this.setState({ lastname: lname })
  }
  getPhone(num) {
    this.setState({ phonenumber: num })
  }
  getEmail(email) {
    this.setState({ email: email })
  }
  getPassword(pass) {
    this.setState({ password: pass })
  }

  onRegister() {
    const { firstname, lastname, email, phonenumber, password } = this.state;
    if (firstname != null || lastname != null || email != null || phonenumber != null || password != null) {
        // this.props.navigation.push('AppDefault');
    }
}

  onLogin = () => {
    // this.props.navigation.push('LoginView');
  }

  render() {
    const br = `\n`;

    return (
      <View style={styles.container}>
        <Image style={styles.logoImage}
          source={require('../Logo/LogoImage.jpg')}
        />
        <Text style={styles.AppName}>Net Coffee {br}</Text>

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: '#'}}/>
          <TextInput style={styles.inputs}
              placeholder="First Name"
              keyboardType="first-name"
              underlineColorAndroid='transparent'
              onChangeText={(fname) => this.getFirstName({fname})}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: '#'}}/>
          <TextInput style={styles.inputs}
              placeholder="Last Name"
              keyboardType="last-name"
              underlineColorAndroid='transparent'
              onChangeText={(lname) => this.getLastName({lname})}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: '#'}}/>
          <TextInput style={styles.inputs}
              placeholder="Phone Number"
              keyboardType="phone-number"
              underlineColorAndroid='transparent'
              onChangeText={(pnumber) => this.getPhone({pnumber})}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: '#'}}/>
          <TextInput style={styles.inputs}
              placeholder="Email"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(email) => this.getEmail({email})}
          />
        </View>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: '#'}}/>
          <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(password) => this.getPassword({password})}
          />
        </View>

        <TouchableHighlight style={[styles.buttonContainer, styles.registerButton]} onPress={() => this.onRegister()}>
          <Text style={styles.loginText}>Register</Text>
        </TouchableHighlight>

        <TouchableHighlight style={styles.buttonContainer}>
            <>
                <Text>Already have an account?</Text>
                <Text onPress={() => this.onLogin()}>  Login</Text>
            </>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  AppName: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#ebab34"
  }, 
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:350,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  logoImage:{
    width: 200,
    height: 200,
  },  
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:320,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#00b5ec",
  },
  registerButton: {
    backgroundColor: "#64E986",
  },
  loginText: {
    color: 'white',
  }
});