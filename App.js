import React from 'react';

import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
} from 'react-native';
import LoginView from './src/Views/LoginView';
import RegisterView from './src/Views/RegisterView';
import AppDefault from './src/Views/AppDefault';

const App: () => React = () => {
  return (
    <>
      <SafeAreaView style={styles.container}>
        {/* <LoginView /> */}
        <RegisterView />
        {/* <AppDefault /> */}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#9dbfb3',
  }
});

export default App;